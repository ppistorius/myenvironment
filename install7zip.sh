#!/bin/bash
#https://www.ruinelli.ch/p7zip-gui-for-linux
set -euxo pipefail

sudo apt-get install wget p7zip-full dolphin -y
sudo apt-get install -y libwxgtk3.0-dev

wget https://www.ruinelli.ch/download/software/p7zip/p7zip_15.09-1_amd64.deb
wget https://www.ruinelli.ch/download/software/p7zip/p7zip-full_15.09-1_amd64.deb
wget https://www.ruinelli.ch/download/software/p7zip/p7zip-gui_15.09-1_amd64.deb
sudo dpkg -i p7zip*15.09*.deb
rm p7zip_15.09-1_amd64.deb p7zip-full_15.09-1_amd64.deb p7zip-gui_15.09-1_amd64.deb
