# Introduction
These scripts are installing all tools I need for my standard setup.
The scripts should work for debian based systems and ubuntu.

On a clean system, you shoulde execute:
```
sudo apt install curl
bash <(curl -s https://gitlab.com/ppistorius/myenvironment/-/raw/master/execOnCleanSystem.sh)
```
