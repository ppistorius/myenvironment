#!/bin/bash

set -euxo pipefail

# check sudo
sudo -v || error "sudo could not be obtained"

sudo apt-get update
sudo apt-get upgrade -y


sudo apt-get install -y git
git clone https://gitlab.com/ppistorius/myenvironment.git
cd myenvironment/
./installmystandardapps.sh noreboot

#prepare installation of guestadditions
sudo apt-get install -y build-essential

set +x
echo "Please install Virtualbox Guestadditions now! After that continue with return!"
read
sudo usermod -a -G vboxsf $USER
echo "Please reboot now!"
read
