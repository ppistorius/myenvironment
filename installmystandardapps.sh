#!/bin/bash

set -euxo pipefail

installdocker() {
    set +e
    sudo apt remove -y docker docker-engine docker.io containerd runc
    sudo apt install -y apt-transport-https ca-certificates curl gnupg lsb-release

    if grep -qi "ubuntu" /etc/os-release; then
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
        echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    elif grep -qi "debian" /etc/os-release; then
        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
        echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    fi

    sudo apt update
    sudo apt install -y docker-ce docker-ce-cli containerd.io

    sudo usermod -aG docker ${USER}
    newgrp docker
    sudo docker run hello-world
}

installdockercompose() {
    # Prüfe die neueste Version von Docker Compose auf GitHub und lade sie herunter
    COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
    sudo curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    # Installiere Bash-Completion für Docker Compose
    sudo curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
}

installgit() {
   sudo apt install git -y
   if ! ls ~/.ssh/id_* 1> /dev/null 2>&1; then    #if no key file exists
      set +x
      echo "git configuration..."
      git config --global core.editor "vim"
      #git config --global --add merge.ff false
      echo "Please enter your first and last name:"
      read gitname 
      echo "Please enter your email:"
      read gitmail 
      git config --global user.name "$gitname"
      git config --global user.email "$gitmail"
      ssh-keygen -o -t ed25519 -a 100 -f ~/.ssh/id_ed25519
      echo
      cat ~/.ssh/id_ed25519.pub
      echo
      read -p "Add the printed public key to your git webaccount and go forward with any key..."
      set -x
   fi
}

installgcp() {
   sudo apt install gcp -y
   if ! grep -q "alias cp=" ~/.zshrc; then
      echo "alias cp=\'gcp\'" >> ~/.zshrc
   fi
}

installzsh() {
   if ! [ -f ~/.zshrc ]; then
      sudo apt install zsh -y
      sudo apt install fonts-powerline -y
      curl -fsSL "https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh" | sh -s -- --unattended
      cp -f configfiles/.zshrc ~
      chsh -s $(which zsh)
   fi
}

installchrome() {
   wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
   sudo apt install ./google-chrome-stable_current_amd64.deb
   rm google-chrome-stable_current_amd64.deb
}

installvenv() {
    python_version=$(python3 --version | awk '{print $2}' | cut -d '.' -f1-2)
    venv_package="python${python_version}-venv"
    sudo apt install -y "${venv_package}"
}

installzsh

# check sudo
sudo -v || error "sudo could not be obtained"
sudo apt update
sudo apt install -y firefox || sudo apt-get install -y firefox-esr
sudo apt install terminator -y
sudo apt install cmake gdb build-essential vim gedit tar sl tree meld net-tools xclip -y
sudo apt install cryptsetup curl netcat-openbsd ssh qemu-system qemu-system-x86 dnsutils wget -y
sudo apt install qtcreator -y
#./install7zip.sh                    #TODO: libs too old!

sudo apt install python3 python3-pip -y
installvenv
sudo apt install snapd -y
sudo snap install atom --classic
sudo snap install pycharm-community --classic

installzsh
installgcp
installgit

installchrome
sudo snap install brackets --classic

installdocker
installdockercompose

if [[ ! "$1" == "noreboot" ]]; then
   set +x
   echo "Its recommended to restart your system after this installtion process. Do you want to restart now? [y/N] "
   read restartnow
   if [[ "$restartnow" == "y" ]]; then
      sudo reboot
   fi
fi


#TODO: vim configure whitespaces, gedit
#TODO: vim tab config
#TODO: test zsh, ohmyzsh and agnoster installation
#TODO: commithook auto trailing whitespace fixing
#TODO: vim autocompletion and tailing whitespace removing
#TODO: script for installation from plain VM with installation of guest additions and loading from these files
#TODO: use functions for subparts ot this skript
#TODO: install docker compose: https://docs.docker.com/compose/install/
#TODO: historysize 100000000000000 

